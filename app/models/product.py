from sqlalchemy import Column, Integer, String, Float
from app.extra.base import BaseModel


class ProductModel(BaseModel):
    __tablename__ = 'product'

    name = Column(String(255))
    brand = Column(String(255))
    price = Column(Float, default=0.0)
    price_pvp = Column(Float, default=0.0)
