from sqlalchemy import Column, Integer, String, Float, ForeignKey
from sqlalchemy.orm import relationship

from app.extra.utils import compute_field
from app.extra.base import BaseModel
from .product import ProductModel
from .party import PartyModel
from .vehicle import VehicleModel


class ShipmentOutModel(BaseModel):
    __tablename__ = 'shipment_out'
    name = Column(String(255))
    reference = Column(String(255))
    total_price = Column(Float, default=0.0)
    party_id = Column(Integer, ForeignKey('party.id',
        name='fk_shipment_out_party_id_party'))
    party = relationship(PartyModel)
    vehicle_id = Column(Integer, ForeignKey('vehicle.id',
        name='fk_shipment_out_vehicle_id_vehicle'))
    vehicle = relationship(VehicleModel)


    @compute_field
    def kilometres(self):
        if self.vehicle:
            return self.vehicle.kilometres
        return ''

    def __repr__(self):
        return self.name


class ShipmentOutLineModel(BaseModel):
    __tablename__ = 'shipment_out_line'

    quantity = Column(Float)
    price = Column(Float)
    shipment_id = Column(Integer, ForeignKey('shipment_out.id',
        name='fk_shipment_out_line_shipment_id_shipment_out'))
    shipment = relationship(ShipmentOutModel)
    product_id = Column(Integer, ForeignKey('product.id',
        name='fk_shipment_out_line_product_id_product'))
    product = relationship(ProductModel)

    def __repr__(self):
        return self.quantity
