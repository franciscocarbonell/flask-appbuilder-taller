from sqlalchemy import Column, Integer, String, ForeignKey
from app.extra.base import BaseModel


class VehicleModel(BaseModel):
    __tablename__ = 'vehicle'

    name = Column(String(255))
    registration = Column(String(255))
    frame = Column(String(255))
    kilometres = Column(Integer)

    def __repr__(self):
        return self.name
