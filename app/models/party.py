from sqlalchemy import Column, Integer, String
from app.extra.base import BaseModel


class PartyModel(BaseModel):
    __tablename__ = 'party'

    name = Column(String(255))
    email = Column(String(255))

    def __repr__(self):
        return self.name