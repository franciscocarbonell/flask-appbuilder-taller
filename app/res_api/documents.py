from flask_appbuilder.api import ModelRestApi
from flask_appbuilder.models.sqla.interface import SQLAInterface
from app.models.documents import ShipmentOutModel
from app import appbuilder


class ShipmentOutApi(ModelRestApi):
    resource_name = 'shipmentoutapi'
    datamodel = SQLAInterface(ShipmentOutModel)

appbuilder.add_api(ShipmentOutApi)