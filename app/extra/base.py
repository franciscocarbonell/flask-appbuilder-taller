from flask_appbuilder import ModelView, Model, expose, has_access
from flask_appbuilder.fieldwidgets import Select2Widget
from wtforms import StringField, DateField
from flask_babel import lazy_gettext as _
from sqlalchemy.inspection import inspect

from flask import request, jsonify
from sqlalchemy import Column, Integer, String, Date
from app.extra.widget import DatePicketWidgetReadOnly
from flask_appbuilder.models.sqla.interface import SQLAInterface


import logging

logger = logging.getLogger(__name__)

def wrapper_error(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as err:
            logger.error(err)
    return wrapper


class TextFieldSelect(StringField):
    def iter_choices(self):
        return[
            ('draft', _('Draft'), False),
            ('active', _('Active'), True)]


class BaseModel(Model):
    __abstract__ = True

    id = Column(Integer, primary_key=True)
    state = Column(String(25))
    created_date = Column(Date)
    modified_date = Column(Date)


class BaseModelView(ModelView):

    def __init__(self, *args, **kwargs):
        if not self.add_form_extra_fields:
            self.add_form_extra_fields = {}
        if not self.edit_form_extra_fields:
            self.edit_form_extra_fields = {}

        updated_dict = {
            'state': TextFieldSelect(_('State'),
                widget=Select2Widget()),
            'created_date': DateField(_('Created date'), widget=DatePicketWidgetReadOnly()),
            'modified_date': DateField(_('Modified date'), widget=DatePicketWidgetReadOnly())
        }
        self.add_form_extra_fields.update(updated_dict)
        self.edit_form_extra_fields.update(updated_dict)

        if not self.label_columns:
            self.label_columns = {}
        self.label_columns.update({
            'state': _('State'),
            'created_date': _('Created date'),
            'modified_date': _('Modified date')
        })
        super().__init__(*args, **kwargs)

    def header_update(self, item):
        pass

    @wrapper_error
    def post_add(self, item):
        self.header_update(item)

    @wrapper_error
    def post_update(self, item):
        self.header_update(item)

    @wrapper_error
    def post_delete(self, item):
        self.header_update(item)

    @expose('/onchange/', methods=['POST'])
    @has_access
    def onchange(self):
        try:
            req_json = request.get_json()
            field_name = req_json.get('field', None)
            field_id = req_json.get('field_id', None)
            onchage_field = req_json.get('onchange_field', None)

            if not field_id or not field_id or not onchage_field:
                return jsonify(error='campos no disponibles'), 400

            record = self.get_relation_model(field_id, field_name)
            if not record:
                return jsonify(error='Datos invalidos'), 400
            return jsonify({onchage_field: getattr(record, onchage_field, None)})
        except Exception as error:
            logger.error(error)
            return jsonify(error=error), 400

    def get_relation_model(self, field_id, field_name):
        view_model = self.datamodel.obj
        relations = dict(inspect(view_model).relationships.items())
        field_relation = relations.get(field_name, None)

        relation_model = field_relation.argument
        interface = SQLAInterface(relation_model, self.datamodel.session)
        record = interface.get(field_id)
        return record
