from flask_appbuilder.fieldwidgets import BS3TextFieldWidget, DatePickerWidget
from datetime import datetime


class TextFieldReadOnly(BS3TextFieldWidget):
    def __call__(self, field, **kwargs):
        kwargs['readonly'] = 'true'
        return super().__call__(field, **kwargs)


class DatePicketWidgetReadOnly(DatePickerWidget):
    # removed class from id-datepicker :: appbuilder_date
    data_template = (
        '<div class="input-group date" id="datepicker">'
        '<span class="input-group-addon"><i class="fa fa-calendar cursor-hand"></i>'
        "</span>"
        '<input class="form-control" data-format="yyyy-MM-dd" %(text)s />'
        "</div>"
    )

    def __call__(self, field, **kwargs):
        kwargs['readonly'] = 'true'
        if not field._value():
            field.data = datetime.today().date()
        return super().__call__(field, **kwargs)


class FloatFieldReadOnly(BS3TextFieldWidget):
    def __call__(self, field, **kwargs):
        kwargs['readonly'] = 'true'
        if not field._value():
            kwargs['value'] = '0.0'
        return super().__call__(field, **kwargs)