from sqlalchemy.ext.hybrid import hybrid_method

class compute_field(hybrid_method):
    def __get__(self, instance, owner):
        if instance:
            return self.func(instance)