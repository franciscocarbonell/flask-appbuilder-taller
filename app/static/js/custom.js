$('body').on('change', '.my_select2_ajax', function(e){
    /*
    console.log(e.added)
    console.log(e.target)
    console.log(e.target.attributes.select_onchange.value)
    console.log(e.target.attributes.name.value)
    */
    var attr = e.target.attributes
    var onchange_view = attr.select_onchange_view.value

    var field = attr.name.value
    var onchange_field = attr.select_onchange.value
    var data = JSON.stringify({
        field: field,
        field_id: e.added.id,
        onchange_field: onchange_field

    })
    var url = `/${onchange_view}/onchange/`
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        dataType: 'json',
        contentType: 'application/json; charset=utf8',
        async: false
    })
    .then(function(result){
        console.log(result)
        $.each(result, function(key, value){
            var selector = `input[name='${key}']`
            $(selector).val(value)
        })
    })
    .catch(function(error){
        console.log(error.responseJSON)
    })
})
