from flask_appbuilder.models.sqla.interface import SQLAInterface
from flask_babel import lazy_gettext as _
from flask_appbuilder.fieldwidgets import BS3TextFieldWidget, Select2AJAXWidget
from wtforms import FloatField, StringField
from flask_appbuilder.fields import AJAXSelectField

from app.models.documents import ShipmentOutModel, ShipmentOutLineModel
from app.extra.widget import FloatFieldReadOnly
from app.extra.base import BaseModelView


class ShipmentLineView(BaseModelView):
    datamodel = SQLAInterface(ShipmentOutLineModel)

    list_title = _('List shipment out lines')
    add_title = _('Add shipment out lines')
    edit_title = _('Edit shipment out lines')

    list_columns = ['state', 'created_date', 'modified_date',
        'shipment', 'product', 'quantity', 'price']
    add_columns = list_columns
    edit_columns = list_columns

    label_columns = {
        'quantity': _('Quantity'),
        'price': _('Price'),
        'shipment': _('Shipment'),
        'product': _('Product')
    }


    def header_update(self, item):
        filters = self.datamodel.get_filters().add_filter(
            'id', self.datamodel.FilterEqual, item.shipment_id)
        count, shipment_lines = self.datamodel.query(filters)
        total = sum([line.quantity * line.price for line in shipment_lines])

        shipment = ShipmentView.datamodel.get(item.shipment_id)
        shipment.total_price = total
        ShipmentView.datamodel.edit(shipment)


class VehicleOnchangeWidget(Select2AJAXWidget):
    def __call__(self, *args, **kwargs):
        kwargs['select_onchange'] = 'kilometres'
        kwargs['select_onchange_view'] = 'shipmentview'
        return super().__call__(*args, **kwargs)


class ShipmentView(BaseModelView):
    datamodel = SQLAInterface(ShipmentOutModel)
    related_views = [ShipmentLineView]

    list_title = _('List shipments out')
    edd_title = _('Add shipments out')
    edit_title = _('Edit shipments out')

    edit_template = 'appbuilder/general/model/edit_cascade.html'

    list_columns = ['state', 'created_date', 'modified_date', 'name',
                    'party', 'vehicle', 'kilometres', 'total_price']
    add_columns = list_columns
    edit_columns = list_columns

    label_columns = {
        'name': _('Name'),
        'party': _('Party')
    }

    add_form_extra_fields = {
        'total_price': FloatField(_('Total price'), widget=FloatFieldReadOnly()),
        'kilometres': StringField(_('kilometres'), widget=BS3TextFieldWidget()),
        'vehicle': AJAXSelectField(
            _('Vehicle'),
            datamodel=datamodel,
            col_name='vehicle',
            widget=VehicleOnchangeWidget('/shipmentview/api/column/add/vehicle'))
    }
    edit_form_extra_fields = add_form_extra_fields


