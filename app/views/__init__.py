from flask import render_template
from app import appbuilder, db
from flask_babel import lazy_gettext as _

from . import party
from . import product
from . import documents
from . import vehicle


@appbuilder.app.errorhandler(404)
def page_not_found(e):
    return (
        render_template(
            "404.html", base_template=appbuilder.base_template, appbuilder=appbuilder
        ),
        404,
    )


db.create_all()

appbuilder.add_view(
    party.PartyModelView,
    'Party',
    label=_("Parties"),
    icon='fa-th',
    category='party',
    category_label=_('Parties'),
    category_icon='fa-th'
)

appbuilder.add_view(
    product.ProductModelView,
    'Product',
    label=_('Product'),
    category_label=_('Product'),
    category='product',
    icon='fa-th',
    category_icon='fa-th'
)

appbuilder.add_view(
    documents.ShipmentView,
    'Shipment',
    label=_('Shipment'),
    category_label=_('Documents'),
    category='documents',
    icon='fa-th',
    category_icon='fa-th'
)

appbuilder.add_view(
    documents.ShipmentLineView,
    'Shipment_line',
    label=_('Shipment Line'),
    category='documents',
    icon='fa-th',
)

appbuilder.add_view(
    vehicle.VehicleModelView,
    'vehicle',
    label=_('Vehicles'),
    category_label=_('Vehicles'),
    category='vehicles',
    icon='fa-th',
    category_icon='fa-th'
)