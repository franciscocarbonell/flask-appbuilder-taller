from flask_appbuilder.models.sqla.interface import SQLAInterface

from app.models.product import ProductModel
from app.extra.base import BaseModelView

from flask_babel import lazy_gettext as _


class ProductModelView(BaseModelView):
    datamodel = SQLAInterface(ProductModel)

    list_title = _('List products')
    add_title = _('Add products')
    edit_title = _('Edit products')

    label_columns = {
        'name': _('Name'),
        'brand': _('Brand'),
        'price': _('Price'),
        'price_pvp': _('Price pvp')
    }

    list_columns = ['state', 'created_date', 'modified_date',
        'name', 'brand', 'price', 'price_pvp']
    add_columns = list_columns
    edit_columns = list_columns
