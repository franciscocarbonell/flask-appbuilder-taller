from flask_appbuilder.models.sqla.interface import SQLAInterface
from flask_babel import lazy_gettext as _

from app.models.party import PartyModel
from app.extra.base import BaseModelView


class PartyModelView(BaseModelView):
    datamodel = SQLAInterface(PartyModel)

    list_title = _('List parties')
    add_title = _('Add parties')
    edit_title = _('Edit parties')

    label_columns = {
        'name': _('Name')
    }

    list_columns = ['state', 'created_date', 'modified_date', 'name']
    add_columns = list_columns
    edit_columns = list_columns
