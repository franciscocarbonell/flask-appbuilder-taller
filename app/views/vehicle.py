from flask_appbuilder.models.sqla.interface import SQLAInterface
from app.extra.base import BaseModelView
from flask_babel import lazy_gettext as _

from app.models.vehicle import VehicleModel
from .documents import ShipmentView


class VehicleModelView(BaseModelView):
    datamodel = SQLAInterface(VehicleModel)
    related_views = [ShipmentView]

    list_title = _('List vehicles')
    add_title = _('Add vehicles')
    edit_title = _('Edit vehicles')

    label_columns = {
        'name': _('Name'),
        'registration': _('Registration'),
        'frame': _('Frame'),
        'kilometres': _('Kilometres')
    }

    list_columns = ['state', 'created_date', 'modified_date', 'name',
        'kilometres', 'registration', 'frame']
    add_columns = list_columns
    edit_columns = list_columns
